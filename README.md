# Spletni barvni stroboskop

1\. domača naloga pri predmetu [Osnove informacijskih sistemov](https://ucilnica.fri.uni-lj.si/course/view.php?id=54) (navodila)


## 6.1 Opis naloge in navodila

Na Bitbucket je na voljo javni repozitorij [https://bitbucket.org/asistent/stroboskop](https://bitbucket.org/asistent/stroboskop), ki vsebuje nedelujoč spletni stroboskop. V okviru domače naloge ustvarite kopijo repozitorija ter popravite in dopolnite obstoječo implementacijo spletne strani tako, da bo končna aplikacija z vsemi delujočimi funkcionalnostimi izgledala kot na spodnji sliki. Natančna navodila si preberite na [spletni učilnici](https://ucilnica.fri.uni-lj.si/mod/workshop/view.php?id=19181).

![Stroboskop](stroboskop.gif)

## 6.2 Vzpostavitev repozitorija

Naloga 6.2.2:

```
git clone https://anzyt@bitbucket.org/anzyt/stroboskop.git
```

Naloga 6.2.3:
https://bitbucket.org/anzyt/stroboskop/commits/7cd0e57fcc6391e38794f12e8310c0fe4faba9d6

## 6.3 Skladnja strani in stilska preobrazba

Naloga 6.3.1:
https://bitbucket.org/anzyt/stroboskop/commits/f3b91d0a53ac20b4cb2229e173062bca1c620773

Naloga 6.3.2:
https://bitbucket.org/anzyt/stroboskop/commits/6effa738db54de71ac4d9cedc7a07c2ce2192039

Naloga 6.3.3:
https://bitbucket.org/anzyt/stroboskop/commits/e731c5a6deab89424142f9bc91d7971c720adfe2

Naloga 6.3.4:
https://bitbucket.org/anzyt/stroboskop/commits/a4fe0423d4b702292f30c45cf9f698147f5c772f

Naloga 6.3.5:

```
git checkout master
git merge izgled
git push
```

## 6.4 Dinamika in animacija na strani

Naloga 6.4.1:
https://bitbucket.org/anzyt/stroboskop/commits/45724075e723dc5de9b6f038e01a695af662d1a5

Naloga 6.4.2:
https://bitbucket.org/anzyt/stroboskop/commits/047fd1b4bc2ad38bcc1dede33c9e9f489d5b3b77

Naloga 6.4.3:
https://bitbucket.org/anzyt/stroboskop/commits/26a2e38d3ceb2b20222c4b7a81d65fd0e629b81e

Naloga 6.4.4:
https://bitbucket.org/anzyt/stroboskop/commits/e3418406e8f88782e84ed55fbcd1c9362fd4950c